#  -*- coding: utf-8 -*-
# """
# Created on Fri Jul 16 14:14:17 2021

# @author: rahee
# """

import paramiko as pk
import time

ssh_client = pk.SSHClient()  # initiate an SSH client
ssh_client.set_missing_host_key_policy(pk.AutoAddPolicy())  # Trust all PC to connect via login/pw
ssh_client.connect('10.3.165.219', username='pi', password='picam')


ssh_client.exec_command('rm /home/pi/Pictures/OT2_images/imagefromcam.jpg')  # delete old image
stdin, stdout, stderr = ssh_client.exec_command('fswebcam /home/pi/Pictures/OT2_images/imagefromcam.jpg')
# capture image on Pi-camera

trans = pk.Transport('10.3.165.219')  # set up sftp
trans.connect(username='pi', password='picam')
ftp = pk.SFTPClient.from_transport(trans)


time.sleep(1)
ftp.get('/home/pi/Pictures/OT2_images/imagefromcam.jpg', 'C:/Users/lab1/pi/imagefromPI.jpg')
# specify the remote location/name of the file, and then specify the local location/name of the file

ftp.close()  # close ftp client connection
ssh_client.close()  # close ssh client connection
