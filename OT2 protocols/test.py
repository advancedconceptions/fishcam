import numpy as np
with open('original_transfer_from_dish.json', 'r') as file:
    filedata = file.read()
xcode = np.linspace(1, 24, 24)
ycode = np.linspace(2, 48, 24)
# Replace the target blob coordinates with calculated ot2 coordinates
for i in range(1,25):
    filedata = filedata.replace('blob[%s]' % (i), '"x":%s, "y":%s' % (xcode[i - 1], ycode[i - 1]))
# Write the file out as a new file
with open('current_transfer_from_dish.json', 'w') as file:
    file.write(filedata)
