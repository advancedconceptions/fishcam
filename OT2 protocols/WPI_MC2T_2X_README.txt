#! README
##########################################
##    MICRO 2T PC Exchange Version 2.B  ##
##    World Precision Instruments, LLC  ##
##                                      ##
##          www.wpiinc.com              ##
##                                      ##
##  This is an example program          ##
##  to show the remote command function ##
##  to remotely control the WPI         ## 
##  MicroTouch2 controller from a       ##
##  PC USB serial port.                 ##
##                                      ##
##                                      ##
##########################################
##  Tested with MicroTouch2 controllers ##
##  and NL2010, NL2020, or UMP3 micro   ## 
##  pumps manufactured after December   ##
##  2019. Older firmware versions       ##
##  may give inconsistent results or    ##
##  not work at all.                    ##
##                                      ##
##########################################
##  Requires Python Execution Program   ##
##           Version 3.2 or higher      ##
##     www.python.org/downloads         ##
##                                      ##
## Added required libraries:            ##
##    1.pySerial load module            ##
##       Install with pip:              ##
##       python -m pip install pyserial ##  
##                                      ##
##    2.keyboard load module            ##
##       Install with pip:              ##
##       python -m pip install keyboard ##
##                                      ##
## Other Imports:                       ##
## sys, time, glob, os[name, system]    ##
##                                      ##
##########################################
##                                      ##
## World Precision Instruments, LLC     ##
## provides this software "as is",      ## 
## without any warranty or guarentee    ##
## of any kind or statement of fitness  ##
## of use. Permission to use, copy,     ##
## modify, and/or distribute this       ##
## software for any purpose without fee ##
## is hereby granted.                   ##
##########################################
## THE SOFTWARE IS PROVIDED "AS IS" AND ##
## THE AUTHOR DISCLAIMS ALL WARRANTIES  ##
## WITH REGARD TO THIS SOFTWARE         ##
## INCLUDING ALL IMPLIED WARRANTIES OF  ##
## MERCHANTABILITY AND FITNESS. IN NO   ##
## EVENT SHALL THE AUTHOR BE LIABLE FOR ##
## ANY SPECIAL, DIRECT,INDIRECT, OR     ##
## CONSEQUENTIAL DAMAGES OR ANY DAMAGES ##
## WHATSOEVER RESULTING FROM LOSS OF    ##
## USE, DATA OR PROFITS, WHETHER IN AN  ##
## ACTION OF CONTRACT, NEGLIGENCE OR    ##
## OTHER TORTIOUS ACTION, ARISING OUT OF##
## OR IN CONNECTION WITH THE USE OR     ##
## PERFORMANCE OF THIS SOFTWARE.        ##
##########################################
##                                      ##  
## (c) WORLD PRECISION INSTRUMENTS, LLC ##
##       AUG 2020                       ##
##                                      ##
##########################################


