#  -*- coding: utf-8 -*-
# """
# Created on Fri Jul 16 14:14:17 2021

# @author: rahee
# """

import paramiko as pk
import time

ssh_client = pk.SSHClient()  # initiate an SSH client
ssh_client.set_missing_host_key_policy(pk.AutoAddPolicy())  # Trust all PC to connect via login/pw
ssh_client.connect('10.3.165.238', username='pi', password='picam')


ssh_client.exec_command('rm /home/pi/Pictures/OT2_images/imagefromcam.jpg')  # delete old image
stdin, stdout, stderr = ssh_client.exec_command('fswebcam /home/pi/Pictures/OT2_images/imagefromcam.jpg')
# capture image on Pi-camera

trans = pk.Transport('10.3.165.238')  # set up sftp
trans.connect(username='pi', password='picam')
ftp = pk.SFTPClient.from_transport(trans)


time.sleep(1)
ftp.get('/home/pi/Pictures/OT2_images/imagefromcam.jpg', 'C:/Users/lab1/pi/imagefromPI.jpg')
# specify the remote location/name of the file, and then specify the local location/name of the file

ftp.close()  # close ftp client connection
ssh_client.close()  # close ssh client connection

time.sleep(5)


import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
from skimage import feature, io, color, filters
import pandas as pd
from skimage.morphology import area_opening


# define function for displaying a single image
def show_image(image, title='Image', cmap_type='gray'):
    plt.imshow(image, cmap=cmap_type)
    plt.title(title)
    plt.axis('off')
    plt.show()


# function to filter out the blobs residing at/outside the petri dish
def blob_filter(blobarray):
    x_max = np.max(blobarray[:, 1]) # get x max value of blobs
    x_min = np.min(blobarray[:, 1])  # get x min value of blobs
    y_min = np.min(blobarray[:, 0])  # get y min value of blobs
    y_max = np.max(blobarray[:, 0])  # get y max value of blobs

    # determine indexes of x_max, y_max, y_min, the '[0][0]' in the end extracts
    # the index value from the tuple
    point1_index = np.where(blobarray[:, 1] == x_max)[0][0]
    point2_index = np.where(blobarray[:, 1] == x_min)[0][0]
    point3_index = np.where(blobarray[:, 0] == y_max)[0][0]
    x = blobarray[:, 1]
    y = blobarray[:, 0]

    y_1 = blobarray[:, 0][point1_index]
    y_2 = blobarray[:, 0][point2_index]
    x_3 = blobarray[:, 1][point3_index]

    x_sel = np.array([])  # empty array for storing x-coordinates of selected blobs
    y_sel = np.array([])  # empty array for storing y-coordinates of selected blobs
    point1 = np.array([y_1, x_max])
    point2 = np.array([y_2, x_min])
    point3 = np.array([y_max, x_3])

# select only blobs that are within the petri dish
    center = np.array([point2[0], point3[1]]) # center is defined as the x value of point 3 and the y value of point 2
    radius = center[0] - point1[0] # radius is defined as the y-distance between the center and point 1
    xpoints = np.array([blobarray[:,1]])
    ypoints = np.array([blobarray[:,0]])
    distx = abs(xpoints[:] - center[1])
    disty = abs(ypoints[:] - center[0])
    radius_sqr = (radius-2)**2
    xsqr = np.array([])
    ysqr = np.array([])
    xsqr = np.append(xsqr, distx[:] ** 2)
    ysqr = np.append(ysqr, disty[:] ** 2)
    sum_sqr = np.array([])
    sum_sqr = np.append(sum_sqr, xsqr[:] + ysqr[:])
    for i in range(0, len(x)):
        if sum_sqr[i] <= radius_sqr* 0.65: # 60% of calculated 'radius' to filter out any false blobs on the edge of petri dish
            x_sel = np.append(x_sel, x[i])
            y_sel = np.append(y_sel, y[i])



    return x_sel, y_sel, point1, point2, point3  # return x, y coordinates of selected blobs and points for calibration beads




# import image
raw_image = io.imread('C:/Users/lab1/pi/imagefromPI.jpg', as_gray=False)

raw_image = np.flipud(raw_image)

# convert image to grayscale
grayscale_image = color.rgb2gray(raw_image)

# obtain optimal threshold value
# thresh = filters.threshold_otsu(grayscale_image)

# create values for following logic
thresh = 0.99
repeat1 = 1
repeat2 = 1
repeat3 = 1
repeat4 = 1

while repeat1 == 1 or repeat2 == 1 or repeat3 == 1 or repeat4 == 1:
    # apply thresholding
    binary = grayscale_image > thresh
    # filter small blobs
    binary = area_opening(binary, 3)

    # filter using LOG filter
    yx_coord = feature.blob_log(binary, min_sigma=3, max_sigma=20, num_sigma=10, threshold=0.1, overlap=0.5)

    # delete blobs outside of a certain area.
    n = 0
    while n < len(yx_coord):
        if yx_coord[n, [0]] < 30 or yx_coord[n, [0]] > 455 or yx_coord[n, [1]] < 60 or yx_coord[n, [1]] > 580:
            yx_coord = np.delete(yx_coord, n, 0)
            n = n - 1
        else:
            n = n + 1

    # run blob filter function
    x_blobs, y_blobs, p1, p2, p3 = blob_filter(yx_coord)  # select the blobs to pick

    # only continue if there are more than three blobs identified
    if len(x_blobs) <= 3:
        repeat1 = 1
    else:
        repeat1 = 0

    # Only continue if the calibration points identified are within an acceptable range of locations
    if 495 < p1[1] < 525 and 65 < p1[0] < 95:
        repeat2 = 0
    else:
        repeat2 = 1
    if 110 < p2[1] < 140 and 200 < p2[0] < 230:
        repeat3 = 0
    else:
        repeat3 = 1
    if 320 < p3[1] < 350 and 390 < p3[0] < 420:
        repeat4 = 0
    else:
        repeat4 = 1

        # if any of the above parameters are not met, repeat the function after reducing the threshold value until calibration points are identified and there are multiple blobs identified
    if repeat1 == 1 or repeat2 == 1 or repeat3 == 1 or repeat4 == 1:
        thresh = thresh - 0.01

# Display the image and mark a red cross on all blobs found
fig, ax = plt.subplots()
ax.imshow(grayscale_image, cmap='gray')
ax.plot(x_blobs, y_blobs, 'r+')
ax.plot(p1[1], p1[0], 'r+')
ax.plot(p2[1], p2[0], 'r+')
ax.plot(p3[1], p3[0], 'r+')

# print values of threshold and location of calibration points for debugging
print(thresh, p1, p2, p3)

# display image
# ax.axis('image')
ax.set_xlim(0, binary.shape[1])
# ax.set_ylim(raw_image.shape[0], 0)
ax.set_ylim(0, binary.shape[0])
plt.show()

# save figure as date and time
from datetime import datetime
now = datetime.now()
name = now.strftime("%m_%d_%H_%M")
fig.savefig("C:/Users/lab1/pi/image_log/%s" % name)

# determine translation coefficient for converting blob coordinates from pixels to mm
def func(x, a, b):
    # for linear curve fit:
    return a * x + b


# for solution of linear equation for x-coordinates
xdata_pixel = np.array([p1[1], p2[1], p3[1]])  # pixel coordinates
xdata_OT = np.array([68.6, 18, 45])  # cartesian coordinates
popt_x, pcov = curve_fit(func, xdata_pixel, xdata_OT)
# print (popt_x)

# for solution of linear equation for y-coordinates
ydata_pixel = np.array([p1[0], p2[0], p3[0]])  # pixel coordinates
ydata_OT = np.array([23.5, 43, 67])  # cartesian coordinates
popt_y, pcov = curve_fit(func, ydata_pixel, ydata_OT)
# print (popt_y)

# convert coordinates from pixels to mm
x_ot = np.array([])  # empty array for storing x-coordinates of blobs in mm
y_ot = np.array([])  # empty array for storing y-coordinates of blobs in mm
for n in range(0, len(x_blobs)):
    x_ot = np.append(x_ot, x_blobs[n] * popt_x[0] + popt_x[1])
    y_ot = np.append(y_ot, y_blobs[n] * popt_y[0] + popt_y[1])

# code to alter the file 'original_transfer_from_dish.json' to make the well positions of embryo_dish
# the positions of the embryos as calculated by this code. This protocol can then be used to
# transfer the embryos without jupyter notebook.
with open('original_transfer_from_dish.json', 'r') as file:
    filedata = file.read()
# Replace the target blob coordinates with calculated ot2 coordinates
for i in range(1, len(x_blobs)):
    filedata = filedata.replace('blob[%s]' % (i), '"x":%s, "y":%s' % (x_ot[i - 1], y_ot[i - 1]))
# If fewer than 24 embryos are observed, the center coordinate will be entered
for i in range(len(x_blobs), 25):
    filedata = filedata.replace('blob[%s]' % (i), '"x":45, "y":43')
fildata = filedata.replace('original_transfer_from_dish', 'current_transfer_from_dish')
# Write the file out as a new file
with open('current_transfer_from_dish.json', 'w') as file:
    file.write(filedata)

# put blob coordinates relative to deck, not deck position
x_otj = x_ot+264.7
y_otj = y_ot+89.8

# creates an empty dataframe and maps it one the data file to remove any earlier entries to the data file
emptyDF = pd.DataFrame(columns=['x_ot', 'y_ot'])
emptyDF.to_csv("C:/Users/lab1/pi/data1.csv")

# convert arrays into dataframe
DF = pd.DataFrame(columns=['x_ot', 'y_ot'])
DF['x_ot'] = x_otj
DF['y_ot'] = y_otj

# save the dataframe as a csv file
DF.to_csv("C:/Users/lab1/pi/data1.csv")
