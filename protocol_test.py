from opentrons import protocol_api
# metadata
metadata = {'protocolName': 'My Protocol',
'author': 'Name <email@address.com>',
'description': 'Simple protocol to get started using OT2',
'apiLevel': '2.10'}
# protocol run function. the part after the colon lets your editor know
# where to look for autocomplete suggestions
def run(protocol: protocol_api.ProtocolContext):
    # labware
    plate = protocol.load_labware('corning_96_wellplate_360ul_flat', '3')
    tiprack = protocol.load_labware('opentrons_96_tiprack_20ul', '9')
    # pipettes
    right_pipette = protocol.load_instrument('p20_single_gen2', 'right', tip_racks=[tiprack])
    # commands
    right_pipette.pick_up_tip()
    right_pipette.aspirate(20, plate['A1'])
    right_pipette.dispense(20, plate['B2'])
    right_pipette.return_tip()
