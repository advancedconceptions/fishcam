
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from skimage import feature, io, color, filters

# define function for displaying a single image
def show_image(image, title='Image', cmap_type='gray'):
    plt.imshow(image, cmap=cmap_type)
    plt.title(title)
    plt.axis('off')
    plt.show()

#function to filter out the blobs residing at/outside the petri dish
def blob_filter(blobarray):

    #x_max = np.max(blobarray[:,1]) # get x max value of blobs
    x_min = np.min(blobarray[:,1]) # get x min value of blobs
    y_min = np.min(blobarray[:,0]) # get y min value of blobs
    y_max = np.max(blobarray[:,0]) # get y max value of blobs
    
    #determine indexes of x_max, y_max, y_min, the '[0][0]' in the end extracts
    #the index value from the tuple
    point1_index = np.where(blobarray[:,0] == y_min)[0][0]
    point2_index = np.where(blobarray[:,1] == x_min)[0][0]
    point3_index = np.where(blobarray[:,0] == y_max)[0][0]
    x = blobarray[:,1]
    y = blobarray[:,0]

    x_1 = blobarray[:,1][point1_index]
    y_2 = blobarray[:,0][point2_index]
    x_3 = blobarray[:,1][point3_index]

    x_sel = np.array([]) # empty array for storing x-coordinates of selected blobs
    y_sel = np.array([]) # empty array for storing y-coordinates of selected blobs
    
    point1 = np.array([y_min, x_1])
    point2 = np.array([y_2, x_min])
    point3 = np.array([y_max, x_3])
   
    
    for n in range(0, len(x)):
        '''
        select only the blobs the lie within a certain  definied below. The 10%
        factor was determined by trial/error to only identify blobs that are not at the
        extreme corners of the petri dish'''
        if (((x[n] - x_1)**2 + (y[n] - y_2)**2) < (0.14*((y_min - y_2)**2 + (x_1 - x_min)**2))):
            x_sel = np.append(x_sel, x[n])
            y_sel = np.append(y_sel, y[n])

    return x_sel, y_sel, point1, point2, point3   # return x, y coordinates of selected blobs and points for calibration beads
  

#import image
raw_image =  io.imread('C:/Users/lab1/pi/imagefromPI.jpg', as_gray= False)

raw_image = np.fliplr(raw_image)


#convert image to grayscale
grayscale_image = color.rgb2gray(raw_image)

#get yx (y coordinate and x coordinate, respectively) coordinates for each blob found
yx_coord = feature.blob_doh(grayscale_image, min_sigma=10, max_sigma=20, num_sigma=10, threshold=0.0005, overlap=0.001)

# delete blobs outside of acceptable dimensions
n = 0
while n < len(yx_coord)+1:
    if yx_coord[n, [0]] < 30 or yx_coord[n, [0]] > 455 or yx_coord[n, [1]] < 50 or yx_coord[n, [1]] > 580:
        yx_coord = np.delete(yx_coord, n, 0)
        n = n-1
    else:
        n = n+1
    if n == len(yx_coord):
        break

#obtain optimal threshold value
thresh = filters.threshold_otsu(grayscale_image)

#apply thresholding
binary = grayscale_image > thresh


# Display the image and mark a red cross on all blobs found
fig, ax = plt.subplots()
ax.imshow(grayscale_image, cmap= 'gray')

for yx in range(0, len(yx_coord)):
    ax.plot(yx_coord[yx, 1], yx_coord[yx, 0], 'r+')

#ax.axis('image')
ax.set_xlim(0, raw_image.shape[1])
#ax.set_ylim(raw_image.shape[0], 0)
ax.set_ylim(0, raw_image.shape[0])
plt.show()

#print(yx_coord)
#print (len(yx_coord))

x_blobs, y_blobs, p1, p2, p3 = blob_filter(yx_coord) #select the blobs to pick

print(p1, p2, p3)





# determine translation coefficient for coverting blob coordinates from pixels to mm
def func (x, a, b):
    #for linear curve fit:
    return a*x + b
    # for quadratic curve fit:
    #return a * np.exp(-b * x) + c
        

# for solution of linear equation for x-coordinates
xdata_pixel = np.array([p1[0], p2[0], p3[0]]) #pixel coordinates
xdata_OT = np.array([332.5, 282.5, 311]) #cartesian coordinates
popt_x, pcov =  curve_fit(func, xdata_pixel, xdata_OT)
# print (popt_x)

# for solution of linear equation for y-coordinates
ydata_pixel = np.array([p1[1], p2[1], p3[1]]) #pixel coordinates
ydata_OT = np.array([22, 42.5, 68]) #cartesian coordinates
popt_y, pcov =  curve_fit(func, ydata_pixel, ydata_OT)
# print (popt_y)


# convert coordinates from pixels to mm
x_ot = np.array([]) # empty array for storing x-coordinates of blobs in mm
y_ot = np.array([]) # empty array for storing y-coordinates of blobs in mm
for n in range(0, len(x_blobs)):
    x_ot = np.append(x_ot, y_blobs[n]*popt_x[0] + popt_x[1]) # y blobs on the picture are actually on the x axes on the OT-2
    y_ot = np.append(y_ot, x_blobs[n]*popt_y[0] + popt_y[1]) # x blobs on the picture are actually on the y axes on the OT-2
    # print('x,y coordinates (mm) ', x_ot[n], y_ot[n])

#get xy coordinates for OT for p1, p2, p3
x_p1 = p1[0]*popt_x[0] + popt_x[1]
y_p1 = p1[1]*popt_y[0] + popt_y[1]
x_p2 = p2[0]*popt_x[0] + popt_x[1]
y_p2 = p2[1]*popt_y[0] + popt_y[1]

#creates an empty dataframe and maps it one the data file to remove any earlier entries to the data file
emptyDF = pd.DataFrame(columns = ['x_ot', 'y_ot'])
emptyDF.to_csv("C:/Users/lab1/pi/data1.csv")

# convert arrays into dataframe
DF = pd.DataFrame(columns = ['x_ot', 'y_ot'])
DF['x_ot'] = x_ot
DF['y_ot'] = y_ot
  
# save the dataframe as a csv file
DF.to_csv("C:/Users/lab1/pi/data1.csv")
